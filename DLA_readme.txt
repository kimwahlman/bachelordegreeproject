1. X-size. (int)
2. Y-size. (int)
3. Caves to generate. (int)
4. Amount of cells to excavate. (int)
5. Divide row 4 further? (int)
6. How many cells can a builder excavate? (int)
7. How many builder can be in the cave at once? (int)
8. Will a builder spawn in the middle, at a random possition or a mix of both? (0 = random, 1 = middle, 2 = middle or random). (int)
9. Can a builder move diagonally? (0 = no, 1 = yes). (int)
10. How many builders shall be created when the digging begins? (int)

Each line here represent the line in the data.txt file.
Place the DLAdata.txt file in the same folder as the .exe file.
Rename it to data.txt.